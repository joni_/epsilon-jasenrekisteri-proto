-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema epsilon_jasenrekisteri
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `epsilon_jasenrekisteri` ;

-- -----------------------------------------------------
-- Schema epsilon_jasenrekisteri
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `epsilon_jasenrekisteri` DEFAULT CHARACTER SET utf8 ;
USE `epsilon_jasenrekisteri` ;

-- -----------------------------------------------------
-- Table `epsilon_jasenrekisteri`.`subjects`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `epsilon_jasenrekisteri`.`subjects` ;

CREATE TABLE IF NOT EXISTS `epsilon_jasenrekisteri`.`subjects` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `subject` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epsilon_jasenrekisteri`.`students`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `epsilon_jasenrekisteri`.`students` ;

CREATE TABLE IF NOT EXISTS `epsilon_jasenrekisteri`.`students` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `student_id` INT NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `birthdate` DATE NOT NULL,
  `subject` INT NOT NULL,
  `startyear` YEAR NOT NULL,
  `active` TINYINT(1) NOT NULL,
  `reference_number` INT NULL,
  `pending` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idusers_UNIQUE` (`id` ASC),
  UNIQUE INDEX `student_id_UNIQUE` (`student_id` ASC),
  INDEX `subject_idx` (`subject` ASC),
  CONSTRAINT `subject`
    FOREIGN KEY (`subject`)
    REFERENCES `epsilon_jasenrekisteri`.`subjects` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epsilon_jasenrekisteri`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `epsilon_jasenrekisteri`.`users` ;

CREATE TABLE IF NOT EXISTS `epsilon_jasenrekisteri`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `resethash` VARCHAR(100) NULL,
  `resettime` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epsilon_jasenrekisteri`.`administration_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `epsilon_jasenrekisteri`.`administration_roles` ;

CREATE TABLE IF NOT EXISTS `epsilon_jasenrekisteri`.`administration_roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(50) NOT NULL,
  `active` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epsilon_jasenrekisteri`.`administration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `epsilon_jasenrekisteri`.`administration` ;

CREATE TABLE IF NOT EXISTS `epsilon_jasenrekisteri`.`administration` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `student_id` INT NOT NULL,
  `role` INT NOT NULL,
  `year` YEAR NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `student_id_idx` (`student_id` ASC),
  INDEX `role_idx` (`role` ASC),
  CONSTRAINT `student_id`
    FOREIGN KEY (`student_id`)
    REFERENCES `epsilon_jasenrekisteri`.`students` (`student_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `role`
    FOREIGN KEY (`role`)
    REFERENCES `epsilon_jasenrekisteri`.`administration_roles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `epsilon_jasenrekisteri`.`emails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `epsilon_jasenrekisteri`.`emails` ;

CREATE TABLE IF NOT EXISTS `epsilon_jasenrekisteri`.`emails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NULL,
  `content` TEXT NOT NULL,
  `sendmail` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `type_UNIQUE` (`type` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `epsilon_jasenrekisteri`.`emails` (`id`, `type`, `title`, `content`, `sendmail`) VALUES (NULL, 'accept', 'Jäsenhakemuksesi on hyväksytty', 'Jäsenhakemuksesi on hyväksytty. Tervetuloa!', '1'), (NULL, 'received', 'Jäsenhakemuksesi on vastaanotettu', 'Jäsenhakemuksesi on vastaanotettu ja se käsitellään mahdollisimman pian.', '1');
