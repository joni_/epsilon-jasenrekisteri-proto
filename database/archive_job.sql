-- Enable scheduler
SET GLOBAL event_scheduler = ON;

DROP EVENT IF EXISTS student_archiever;

-- Event creation
CREATE EVENT student_archiever
ON SCHEDULE EVERY 1 YEAR
STARTS '2015-08-15 03:00:00'
DO
UPDATE students
SET active = 0
WHERE abs(datediff(str_to_date(concat(startyear, "-01-01"), "%Y-%c-%d"), date(now()))/365) >= 7;
