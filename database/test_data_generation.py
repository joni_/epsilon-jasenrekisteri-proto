import random

""" Generates SQL statements for test data insertions """


def generate(n=50):
    subjects = ['Fy', 'Tkt', 'Ma']
    for s in subjects:
        print("INSERT INTO `epsilon_jasenrekisteri`.`subjects` (`subject`) " +
              "VALUES ('{0}');".format(s))

    roles = ['Puheenjohtaja', 'Sihteeri', 'Tapahtumavastaava']
    for r in roles:
        print("INSERT INTO `epsilon_jasenrekisteri`.`administration_roles` " +
              "(`role`, `active`) VALUES ('{0}', 1);".format(r))

    ids = []
    while len(ids) < n:
        student_id = random.randint(10000, 99999)
        if (student_id in ids):
            continue
        ids.append(student_id)
        subj = random.randint(1, 3)
        name = get_random_name()
        email = '.'.join(name.split()) + '@student.uef.fi'
        date = get_random_date()
        startyear = random.randint(2005, 2014)
        active = random.randint(0, 1)
        pending = random.randint(0, 1)
        print("INSERT INTO `epsilon_jasenrekisteri`.`students` " +
              "(`student_id`, `name`, `email`, " +
              "`birthdate`, `subject`, `startyear`, `active`, `pending`) " +
              "VALUES ({0}, '{1}', '{2}', ".format(student_id, name, email) +
              "'{0}', {1}, {2}, {3}, ".format(date, subj, startyear, active) +
              "{0});".format(pending))

    x = random.randint(int(n/3), n)
    for i in range(x):
        student_id = random.randint(1, n)
        role = random.randint(1, 3)
        year = random.randint(2008, 2014)
        print("INSERT INTO `epsilon_jasenrekisteri`.`administration` " +
              "(`student_id`, `role`, `year`) " +
              "VALUES ((SELECT student_id " +
              "FROM students WHERE students.id = {0}),".format(student_id) +
              "{0}, {1});".format(role, year))

    print("INSERT INTO `epsilon_jasenrekisteri`.`users` " +
          "(`username`, `password`, `email`) " +
          "VALUES ('joni', " +
          "'$2y$10$yQwV3eSX8SzR3c.uaYGO6ubEIm0qpDqWlTx90T56jxVXwLRA8b6bu', " +
          "'ignorethis');")

    print("INSERT INTO `epsilon_jasenrekisteri`.`users` " +
          "(`username`, `password`, `email`) " +
          "VALUES ('epsilon', " +
          "'$2y$10$yQwV3eSX8SzR3c.uaYGO6ubEIm0qpDqWlTx90T56jxVXwLRA8b6bu', " +
          "'ignorethis');")


def get_random_name():
    firstNames = ['Jarmo', 'Jarno', 'Jere', 'Jimmy', 'Joni', 'Joonas',
                  'Joonas', 'Jose', 'Jouni', 'Juha', 'Juha', 'Juha', 'Juha',
                  'Juha', 'Juha', 'Juhani', 'Juho', 'Juho', 'Juho', 'Juho',
                  'Jukka', 'Juri', 'Jussi', 'Jussi', 'Jussi', 'Kai', 'Kai',
                  'Kalle', 'Kalle', 'Kalle', 'Karri', 'Kristian', 'Kuisma',
                  'Lars', 'Lassi', 'Lauri', 'Lauri', 'Leevi', 'Marko', 'Marko']
    lastNames = ['Kulo', 'Lehto', 'Lahdensuo', 'Hirvensalo', 'Kalima',
                 'Leikola', 'Leppälä', 'Linko', 'Loimaranta', 'Louhivuori',
                 'Maasalo', 'Malmivaara', 'Merikanto', 'Mannermaa', 'Kivalo',
                 'Muroma', 'Nevanlinna', 'Nuorivaara', 'Nuorteva', 'Suviranta',
                 'Suolahti']
    firstname = firstNames[random.randint(0, len(firstNames)-1)]
    lastname = lastNames[random.randint(0, len(lastNames)-1)]
    return "{0} {1}".format(firstname, lastname)


def get_random_date():
    year = random.randint(1985, 1995)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return "{0}-{1}-{2}".format(year, month, day)


generate(50)
