var vm = new MainViewModel();
ko.applyBindings(vm);

var loadStartPage = function() {
    $.get(epsilonConfig.api.loggedInUrl, function(response) {
        var data = JSON.parse(response);
        epsilonConfig.loadConfig();
        if (data.loggedin) {
            loadStudents();
            vm.activateStudentListing();
            vm.showNavigation();
            loadPendingStudents();
        } else {
            vm.activateLoginPage();
        }
    });
};

var loadPendingStudents = function() {
    $.get(epsilonConfig.api.pendingStudentsUrl, function(data) {
        var students = ko.utils.arrayMap(data, function(student) {
            return new Student(
                student.id,
                student.name,
                student.birthdate,
                student.email,
                student.subject,
                student.startyear,
                student.hallitus,
                student.active,
                student.hallitusRoles
            );
        });
        vm.pendingStudentsModel.pendingStudents(students);
    });
};

var loadStudents = function() {
    $.get(epsilonConfig.api.studentsUrl, function(data) {
        var students = ko.utils.arrayMap(data, function(student) {
            return new Student(
                student.id,
                student.name,
                student.birthdate,
                student.email,
                student.subject,
                student.startyear,
                student.hallitus,
                student.active,
                student.hallitusRoles
            );
        });
        vm.studentListing.sortStudentsBy('id');
        vm.studentListing.students(students);
    });
};

loadStartPage();

