var PendingStudentsModel = function() {
    var self = this;

    self.pendingStudents = ko.observableArray([]);

    self.acceptStudent = function(student) {
        self.acceptStudents([student.id()]);
    };

    self.rejectStudent = function(student) {
        var result = window.confirm('Haluatko varmasti hylätä opiskelijan ' + 
            student.name() + '?');
        if (result) {
            self.rejectStudents([student.id()]);
        }
    };

    self.studentAcceptedHandler = function(student) {};
    self.studentRejectedHandler = function(student) {};

    self.fadeOutRow = function(node, index, element) {
        $(node).fadeOut(500, function() {
            $(node).remove();
        });
    };

    self.allSelected = ko.pureComputed({
        read: function() {
            var firstUnchecked = ko.utils.arrayFirst(
                self.pendingStudents(), 
                function(student) {
                    return student.isSelected() === false;
                }
            );
            return firstUnchecked === null;
        },
        write: function(value) {
            ko.utils.arrayForEach(
                self.pendingStudents(), 
                function(student) {
                    student.isSelected(value);
                }
            );
        }
    });

    self.acceptSelectedStudents = function() {
        var result = window.confirm('Hyväksytäänkö kaikki valitut hakemukset?');
        if (result) {
            var ids = [];
            ko.utils.arrayForEach(self.selectedStudents(), function(student) {
                ids.push(student.id());
            });
            self.acceptStudents(ids);
        }
    };

    self.selectedStudents = ko.pureComputed(function() {
        var selected = [];
        ko.utils.arrayForEach(self.pendingStudents(), function(student) {
            if (student.isSelected()) {
                selected.push(student);
            }
        });
        return selected;
    });

    self.acceptStudents = function(studentIds) {
        var input = {idList: studentIds};
        $.post(epsilonConfig.api.acceptStudentUrl, ko.toJSON(input), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                var toRemove = ko.utils.arrayFilter(self.pendingStudents(), function(student) {
                    return studentIds.indexOf(student.id()) !== -1;
                });
                ko.utils.arrayForEach(toRemove, function(student) {
                    self.pendingStudents.remove(student);
                    self.studentAcceptedHandler(student);
                    student.isSelected(false);
                });
            } else {
                alert('Hyväksyminen epäonnistui');
            }
        });
    };

    self.rejectSelectedStudents = function() {
        var result = window.confirm('Hylätäänkö kaikki valitut hakemukset?');
        if (result) {
            var ids = [];
            ko.utils.arrayForEach(self.selectedStudents(), function(student) {
                ids.push(student.id());
            });
            self.rejectStudents(ids);
        }
    };

    self.rejectStudents = function(studentIds) {
        var input = {idList: studentIds};
        $.post(epsilonConfig.api.rejectStudentUrl, ko.toJSON(input), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                var toRemove = ko.utils.arrayFilter(self.pendingStudents(), function(student) {
                    return studentIds.indexOf(student.id()) !== -1;
                });
                ko.utils.arrayForEach(toRemove, function(student) {
                    self.studentRejectedHandler(student);
                    self.pendingStudents.remove(student);
                    student.isSelected(false);
                });
            } else {
                alert('Hakemusten hylkääminen epäonnistui');
            }
        });
    };
};