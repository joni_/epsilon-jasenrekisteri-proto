var LoginViewModel = function() {
    var self = this;
    
    self.username = ko.observable('');
    self.password = ko.observable('');

    self.login = function() {
        self.hideErrors();
        var data = {username: self.username(), password: self.password()};
        $.post(epsilonConfig.api.loginUrl, ko.toJSON(data), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                self.afterLoginHandler();
            } else {
                self.displayError();
            }
        });
    };

    self.afterLoginHandler = function() {};

    self.displayError = function() {
        $('#errors').show();
    };

    self.hideErrors = function() {
        $('#errors').hide();
    };

    self.requiredFieldsFilled = ko.pureComputed(function() {
        return self.username() && self.password();
    });

    self.adminEmail = ko.observable('');

    self.passwordResetVisible = ko.observable(false);
    self.togglePasswordReset = function() {
        self.passwordResetVisible(!self.passwordResetVisible());
    };

    self.showPasswordResetSuccess = ko.observable(false);
    self.showPasswordResetErrors = ko.observable(false);
    self.resetSuccessText = ko.observable('');
    self.resetErrorsText = ko.observable('');
    self.resetPassword = function() {
        self.showPasswordResetSuccess(false);
        self.showPasswordResetErrors(false);

        var input = { email: self.adminEmail() };
        $.post(epsilonConfig.api.resetPasswordInit, ko.toJSON(input), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                self.resetSuccessText(data.message);
                self.showPasswordResetSuccess(true);
            } else {
                self.resetErrorsText(data.message);
                self.showPasswordResetErrors(true);               
            }
        });        
    };
};