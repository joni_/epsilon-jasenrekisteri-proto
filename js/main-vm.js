var MainViewModel = function() {
    var self = this;
    self.studentListing = new StudentListingViewModel();
    self.studentListingView = new View(
        'studentlisting', 'studentlisting', self.studentListing);

    self.editStudent = new EditStudentViewModel();
    self.editStudent.doneEditingHandler = function() {
        self.studentListing.sortAgain();
        self.studentListingView.loadView();
    };

    self.editStudentView = new View('editstudent', 'editstudent', 
        self.editStudent);

    self.studentListing.editStudentHandler = function(student) {
        self.editStudent.startEditing(student);
        self.editStudentView.loadView();
    };
    
    self.activateStudentListing = function() {
        self.studentListingView.loadView();
    };

    self.addStudentModel = new EditStudentViewModel();
    self.addStudentModel.doneEditingHandler = function(student) {
        if (student) {
            self.studentListing.students().push(student);
            self.studentListing.sortAgain();
        }
        self.studentListingView.loadView();
    };
    self.addStudentView = new View('editstudent', 'editstudent', self.addStudentModel);

    self.addStudent = function() {
        self.addStudentModel.startEditing(new Student());
        self.addStudentView.loadView();
    };

    self.loginPageModel = new LoginViewModel();
    self.loginPageModel.afterLoginHandler = function() {
        epsilonConfig.loadConfig();
        self.showNavigation();
        self.studentListingView.loadView();
        loadStudents();
        loadPendingStudents();
    };

    self.loginPageView = new View('login', 'login', self.loginPageModel);
    self.activateLoginPage = function() {
        self.hideNavigation();
        self.loginPageView.loadView();
    };

    self.hideNavigation = function() {
        $('#navigation').hide();
    };

    self.showNavigation = function() {
        $('#navigation').show();
    };

    self.logout = function() {
        $.get(epsilonConfig.api.logoutUrl, function() {
            self.activateLoginPage();
        });
    };

    self.pendingStudentsModel = new PendingStudentsModel();
    self.pendingStudentsModel.studentAcceptedHandler = function(student) {
        self.studentListing.students().push(student);
        self.studentListing.sortAgain();

        if (self.studentsPendingCount() === 0) {
            self.studentListingView.loadView();
        }
    };
    self.pendingStudentsModel.studentRejectedHandler = function(student) {
        if (self.studentsPendingCount() === 0) {
            self.studentListingView.loadView();
        }
    };

    self.pendingStudentsView = new View('pending-students', 
        'pendingstudents', self.pendingStudentsModel);
    
    self.activatePendingStudentsPage = function() {
        self.pendingStudentsView.loadView();
    };
    
    self.studentsPendingCount = ko.pureComputed(function() {
        return self.pendingStudentsModel.pendingStudents().length;
    });

    self.settingsViewModel = new SettingsViewModel();
    self.settingsView = new View('settings', 'settings', self.settingsViewModel);
    self.activateSettingsPage = function() {
        self.settingsView.loadView();
    };
};
