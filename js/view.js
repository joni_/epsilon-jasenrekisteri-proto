var View = function(id, template, data) {
    var self = this;
    self.id = id;
    self.template = template;
    self.data = data;

    self.loadView = function() {
        var templatePath = 'templates/' + self.template + '.tmpl.html';
        $('#container').load(templatePath, function() {
            ko.applyBindings(self.data, document.getElementById(self.id));
        });
    };
};
