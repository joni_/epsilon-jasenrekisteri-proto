var epsilonConfig = {
    subjects: ['Ma', 'Fy', 'Tkt'],
    roles: ko.observableArray([]),
    emails: ko.observable({
        received: {title: '', content: '', sendmail: true},
        accept: {title: '', content: '', sendmail: true}
    }),
    adminEmail: ko.observable('admin@mail.com'),

    api: {
        studentsUrl: 'http://localhost/jasenrekisteri/api/students',
        pendingStudentsUrl: 'http://localhost/jasenrekisteri/api/students/pending',
        removeStudentsUrl: 'http://localhost/jasenrekisteri/api/students/remove',
        acceptStudentUrl: 'http://localhost/jasenrekisteri/api/students/pending/accept',
        rejectStudentUrl: 'http://localhost/jasenrekisteri/api/students/pending/reject',
        loggedInUrl: 'http://localhost/jasenrekisteri/api/auth/loggedin',
        loginUrl: 'http://localhost/jasenrekisteri/api/auth/login',
        logoutUrl: 'http://localhost/jasenrekisteri/api/auth/logout',
        registerUrl: 'http://localhost/jasenrekisteri/api/register',
        configFetchUrl: 'http://localhost/jasenrekisteri/api/config',
        resetPasswordInit: 'http://localhost/jasenrekisteri/api/auth/reset/init',
        hashValidationUrl: 'http://localhost/jasenrekisteri/api/auth/reset/validhash',
        resetPasswordUrl: 'http://localhost/jasenrekisteri/api/auth/reset/reset'
    }
};

epsilonConfig.loadConfig = function() {
    $.get(epsilonConfig.api.configFetchUrl, function(response) {
        var data = JSON.parse(response);
        if (data.success) {
            epsilonConfig.roles(data.roles);
            epsilonConfig.emails(data.emails);
            epsilonConfig.adminEmail(data.adminEmail);
        }
    });
};
