var StudentListingViewModel = function() {
    var self = this;

    self.students = ko.observableArray([]);
    self.idFilter = ko.observable('');
    self.nameFilter = ko.observable('');
    self.yearFilterStart = ko.observable('');
    self.yearFilterEnd = ko.observable('');
    self.subjectFilter = ko.observable('');
    self.onlyActiveFilter = ko.observable(true);

    self.hallitusRoleFilter = ko.observable('');
    self.hallitusFilter = ko.pureComputed(function() {
        return self.hallitusRoleFilter() !== 'Ei suodatusta';
    });

    self.selectableSubjects = function() {
        var subjects = [""];
        ko.utils.arrayForEach(epsilonConfig.subjects, function(subject) {
            subjects.push(subject);
        });
        return subjects.sort();
    }();

    self.selectableRoles = ko.pureComputed(function() {
        var roles = [];
        ko.utils.arrayForEach(epsilonConfig.roles(), function(role) {
            roles.push(role.role);
        });
        roles.sort();
        roles.unshift("Ei suodatusta", "Kaikki hallituksen jäsenet");
        return roles;
    });

    self.clearFilters = function() {
        self.idFilter('');
        self.nameFilter('');
        self.yearFilterStart('');
        self.yearFilterEnd('');
        self.subjectFilter('');
        self.hallitusRoleFilter('Ei suodatusta');
        self.onlyActiveFilter(true);
    };

    self.minYear = ko.pureComputed(function() {
        var year = Number.MAX_SAFE_INTEGER;
        ko.utils.arrayForEach(self.students(), function(student) {
            if (student.startyear() < year) {
                year = student.startyear();
            }
        });
        return year;
    });

    self.maxYear = ko.pureComputed(function() {
        var year = 0;
        ko.utils.arrayForEach(self.students(), function(student) {
            if (student.startyear() > year) {
                year = student.startyear();
            }
        });
        return year;
    });

    self.years = ko.pureComputed(function() {
        var years = [''];
        for (var year = self.minYear(); year <= self.maxYear(); year++) {
            years.push(year);
        }
        return years;
    });

    self.printSelected = function() {
        ko.utils.arrayForEach(self.filteredStudents(), function(student) {
            if (student.isSelected()) {
                console.log(student.name());
            }
        });
    };

    self.removeSelectedStudents = function() {
        var selectedStudents = ko.utils.arrayFilter(
            self.filteredStudents(),
            function(student) {
                return student.isSelected();
            });

        if (selectedStudents.length === 0) { return; }

        var result = window.confirm('Haluatko varmasti poistaa valitut ' + 
            selectedStudents.length + ' opiskelijaa?');
        if (result) {
            var studentIds = [];
            ko.utils.arrayForEach(selectedStudents, function(student) {
                studentIds.push({id: student.id()});
            });

            $.post(epsilonConfig.api.removeStudentsUrl, ko.toJSON(studentIds), function(response) {
                var data = JSON.parse(response);
                if (data.success) {
                    ko.utils.arrayForEach(selectedStudents, function(student) {
                        self.students.remove(student);
                    });                    
                } else {
                    alert('Valittujen opiskelijoiden poistaminen epäonnistui');
                }
            });
        }
    };

    self.removeStudent = function(student) {
        var result = window.confirm('Haluatko varmasti poistaa opiskelijan ' + 
            student.name() + '?');
        if (result) {
            var studentIds = [{id: student.id}];
            $.post(epsilonConfig.api.removeStudentsUrl, ko.toJSON(studentIds), function(response) {
                var data = JSON.parse(response);
                if (data.success) {
                    self.students.remove(student);
                } else {
                    alert('Valitun opiskelijan poistaminen epäonnistui');
                }
            });
        }
    };

    self.allSelected = ko.pureComputed({
        read: function() {
            var firstUnchecked = ko.utils.arrayFirst(
                self.paginatedStudents(), 
                function(student) {
                    return student.isSelected() === false;
                }
            );
            return firstUnchecked === null;
        },
        write: function(value) {
            ko.utils.arrayForEach(
                self.paginatedStudents(), 
                function(student) {
                    student.isSelected(value);
                }
            );
        }
    });

    self.editStudentHandler = null;
    self.editStudent = function(student) {
        if (self.editStudentHandler) {
            self.editStudentHandler(student);
        }
    };

    self.numOfSelectedStudents = ko.pureComputed(function() {
        var count = 0;
        ko.utils.arrayForEach(self.filteredStudents(), function(student) {
            if (student.isSelected()) {
                count++;
            }
        });
        return count;
    }, self);

    self.pageSize = ko.observable(50);
    self.pageIndex = ko.observable(0);

    self.changePage = function(index) {
        self.pageIndex(index.i);
    };

    self.previousPage = function() {
        if (self.pageIndex() !== 0) {
            self.pageIndex(self.pageIndex() - 1);
        }
    };
    
    self.isCurrentPage = function(index) {
        return index == self.pageIndex();
    };

    self.nextPage = function() {
        if (self.pageIndex() !== self.maxPageIndex()) {
            self.pageIndex(self.pageIndex() + 1);
        }
    };

    self.resetPagination = function() {
        self.pageIndex(0);
    };

    self.filteredStudents = ko.computed(function() {
        self.resetPagination();

        var containsFilter = {
            id: self.idFilter(),
            name: self.nameFilter(),
            subject: self.subjectFilter()
        };

        var yearFiltersExist = self.yearFilterStart() || self.yearFilterEnd();

        var equalsFilters = {
            hallitus: self.hallitusFilter(),
            active: self.onlyActiveFilter()
        };

        if (!containsFilter.id && !containsFilter.name && !containsFilter.subject && !yearFiltersExist && !equalsFilters.hallitus && !equalsFilters.active) {
            return self.students();
        } else {
            return ko.utils.arrayFilter(self.students(), function(student) {
                var match = student.matchesContainsFilters(containsFilter) && 
                        student.matchesYears(self.yearFilterStart(), self.yearFilterEnd()) &&
                        student.matchesEqualsFilters(equalsFilters);

                if (match && self.hallitusRoleFilter() !== "Ei suodatusta" && self.hallitusRoleFilter() !== "Kaikki hallituksen jäsenet") {
                    match = student.matchesHallitusFilter(self.hallitusRoleFilter());
                }

                return match;
            });
        }
    }, self);
        
    self.maxPageIndex = ko.computed(function() {
        var itemsPerPage = self.pageSize();
        var items = self.filteredStudents().length;
        return Math.ceil(items / itemsPerPage) - 1;
    });

    self.pageIndexes = ko.computed(function() {
        var result = [];
        for (var i = 0; i <= self.maxPageIndex(); i++) {
            result.push({i: i});
        }
        return result;
    });

    self.paginatedStudents = ko.computed(function() {
        var size = self.pageSize();
        var start = self.pageIndex() * size;
        return self.filteredStudents().slice(start, start + size);
    });

    self.numOfStudents = ko.pureComputed(function() {
        return self.filteredStudents().length;
    }, self);

    self.sortedBy = 'id';
    self.sortedAsc = false;

    self.sortByStudentNumber = function() {
        self.sortStudentsBy('id');
    };

    self.sortByName = function() {
        self.sortStudentsBy('name');
    };

    self.sortByBirthdate = function() {
        self.sortStudentsBy('birthdate');
    };

    self.sortBySubject = function() {
        self.sortStudentsBy('subject');
    };

    self.sortByStartingYear = function() {
        self.sortStudentsBy('startyear');
    };

    self.sortAgain = function() {
        self.sortedAsc = !self.sortedAsc;
        self.sortStudentsBy(self.sortedBy);
    };

    self.sortStudentsBy = function(property) {
        // When first sorted, order it ascending
        if (self.sortedBy == property && self.sortedAsc) {
            self.students.sort(compare('-' + property));
            self.sortedAsc = false;
        } else {
            self.students.sort(compare(property));
            self.sortedAsc = true;
        }
        self.sortedBy = property;
    };

    self.activeStudentCount = ko.pureComputed(function() {
        var count = 0;
        ko.utils.arrayForEach(self.students(), function(student) {
            if (student.active()) {
                count++;
            }
        });
        return count;
    });

    self.inactiveStudentCount = ko.pureComputed(function() {
        return self.students().length - self.activeStudentCount();
    });

    self.totalStudentCount = ko.pureComputed(function() {
        return self.activeStudentCount() + self.inactiveStudentCount();
    });
};

function compare(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var aValue = a[property]();
        var bValue = b[property]();

        if (property == 'birthdate') {
            aValue = getDateFromString(aValue);
            bValue = getDateFromString(bValue);
        } else if (property == 'id' || property == 'startyear') {
            aValue = parseInt(aValue);
            bValue = parseInt(bValue);
        } else if (property == 'name') {
            // When sorting by name, sort by lastname
            aValue = getLastName(aValue);
            bValue = getLastName(bValue);
        }

        var result = (aValue < bValue) ? -1 : (aValue > bValue) ? 1 : 0;
        return result * sortOrder;
    };
}

function getDateFromString(dateString) {
    var parts = dateString.split(".");
    var day = parseInt(parts[0]);
    var month = parseInt(parts[1]);
    var year = parseInt(parts[2]);
    return new Date(year, month, day);
}

function getLastName(name) {
    var parts = name.split(" ");
    return parts.length < 1 ? name : parts[1];
}
