var PasswordResetViewModel = function() {
    var self = this;

    self.allowReset = ko.observable(false);

    self.password = ko.observable('');
    self.passwordVerification = ko.observable('');

    self.validPasswords = ko.computed(function() {
        var areSame = self.password() == self.passwordVerification();
        var validLength = self.password().length >= 8;
        return areSame && validLength;
    });

    self.resetPassword = function() {
        var hash = getParameterByName('h');
        var input = { hash: hash, password: self.password() };
        $.post(epsilonConfig.api.resetPasswordUrl, ko.toJSON(input), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                self.password('');
                self.passwordVerification('');   
            }
            alert(data.message);
        });
    };
};

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var vm = new PasswordResetViewModel();

// Get hash from query
var hash = getParameterByName("h");

// Send request to API if user can change the password
var input = { hash: hash };
$.get(epsilonConfig.api.hashValidationUrl, input, function(response) {
    var data = JSON.parse(response);
    if (data.success) {
        vm.allowReset(true);
    } else {
        vm.allowReset(false);
    }
});

ko.applyBindings(vm);