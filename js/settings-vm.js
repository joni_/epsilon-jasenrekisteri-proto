var SettingsViewModel = function() {
    var self = this;

    self.administrativeRoles = epsilonConfig.roles;

    self.newRole = ko.observable('');
    self.addNewRole = function() {
        var role = self.newRole();
        var valid = true;

        ko.utils.arrayForEach(self.administrativeRoles(), function(roleEntry) {
            if (roleEntry.role.toLowerCase() === role.toLowerCase()) {
                valid = false;
            }
        });

        if (valid) {
            self.administrativeRoles.push({role: self.newRole(), active: true});
        } else {
            alert('Rooli ' + role + ' on jo olemassa');
        }
    };

    self.saveSettings = function() {
        self.showSuccess(false);
        self.showErrors(false);

        var input = { 
                    roles: self.administrativeRoles,
                    emails: {
                        received: { sendmail: self.sendReceivedConfirmation, title: self.registrationReceivedTitle, content: self.registrationReceived },
                        accepted: { sendmail: true, title: self.registrationAcceptedTitle, content: self.registrationAccepted }
                    },
                    adminEmail: self.adminEmail
        };

        if (self.password()) {
            input.password = self.password();
        }

        $.post(epsilonConfig.api.configFetchUrl, ko.toJSON(input), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                epsilonConfig.roles(self.administrativeRoles());
                self.showSuccess(true);
            } else {
                self.showErrors(true);
            }
        });
    };

    self.showSuccess = ko.observable(false);
    self.showErrors = ko.observable(false);

    self.registrationReceived = ko.observable('');
    self.registrationReceivedTitle = ko.observable('');
    self.registrationAccepted = ko.observable('');
    self.registrationAcceptedTitle = ko.observable('');
    self.sendReceivedConfirmation = ko.observable(true);
    self.adminEmail = ko.observable('');

    self.updateConfig = ko.computed(function() {
        self.registrationReceived(epsilonConfig.emails().received.content);
        self.registrationReceivedTitle(epsilonConfig.emails().received.title);
        self.registrationAccepted(epsilonConfig.emails().accept.content);
        self.registrationAcceptedTitle(epsilonConfig.emails().accept.title);
        self.sendReceivedConfirmation(epsilonConfig.emails().received.sendmail);
    
        self.adminEmail(epsilonConfig.adminEmail());
    });

    self.password = ko.observable('');
    self.passwordVerification = ko.observable('');

    self.validPasswords = ko.computed(function() {
        if (!self.password()) {
            return true;
        }
        var minLength = 8;
        var validLenght = self.password().length >= minLength;
        var passwordMatch = self.password() === self.passwordVerification();
        return validLenght && passwordMatch;
    });
};