var EditStudentViewModel = function() {
    var self = this;
    self.originalStudent = new Student();
    self.editStudent = ko.observable(null);

    self.selectableYears = function() {
        var years = [2000];
        var currentYear = new Date().getFullYear();
        for (var year = years[0]+1; year <= currentYear; year++) {
            years.push(year);
        }
        return years;
    }();

    self.selectableRoles = ko.computed(function() {
        roles = [];
        ko.utils.arrayForEach(epsilonConfig.roles(), function(role) {
            if (role.active) {
                roles.push(role.role);
            }
        });
        return roles;
    });

    self.selectableSubjects = epsilonConfig.subjects;

    self.startEditing = function(student) {
        self.originalStudent = student;
        self.editStudent(student.getCopy());
    };

    self.doneEditingHandler = null;
    self.submitEdits = function() {
        // Send student to server
        $.post(epsilonConfig.api.studentsUrl, ko.toJSON(self.editStudent), function(response) {
            var data = JSON.parse(response);
            if (data.success) {
                // Apply changes
                self.originalStudent.syncStudent(self.editStudent);
                if (self.doneEditingHandler) {
                    self.doneEditingHandler(self.originalStudent);
                }                
            } else {
                alert('Päivitys epäonnistui');
            }
        });
    };

    self.cancelEdits = function() {
        if (self.doneEditingHandler) {
            self.doneEditingHandler();
        }
    };

    self.validData = ko.pureComputed(function() {
        if (!self.editStudent()) return false;

        var validId = self.editStudent().id.isValid();
        var validName = self.editStudent().name.isValid();
        var validBirthdate = self.editStudent().birthdate.isValid();
        var validEmail = self.editStudent().email.isValid();
        return validId && validName && validBirthdate && validEmail;
    });

    self.newRoleRow = function() {
        self.editStudent().hallitusRoles.push({year: null, role: null});
    };

    self.removeAdministrationRole = function(role) {
        self.editStudent().hallitusRoles.remove(role);
    };
};
