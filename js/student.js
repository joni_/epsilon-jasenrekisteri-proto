var Student = function(id, name, birthdate, email, subject, startyear, hallitus, active, hallitusRoles) {
    var self = this;

    active = typeof active !== 'undefined' ? active : true; 
    hallitus = typeof hallitus !== 'undefined' ? hallitus : false;

    self.id = ko.observable(id).extend(
        { 
            required: true,
            number: true
        }
    );
    self.name = ko.observable(name).extend({ required: true });
    self.birthdate = ko.observable(birthdate).extend({ required: true });
    self.email = ko.observable(email).extend(
        {
            required: true,
            email: true
        }
    );
    self.subject = ko.observable(subject);
    self.startyear = ko.observable(startyear);
    self.hallitus = ko.observable(hallitus);
    self.active = ko.observable(active);
    self.hallitusRoles = ko.observableArray(hallitusRoles);

    self.isSelected = ko.observable(false);

    self.getCopy = function() {
        return new Student(
            self.id(),
            self.name(),
            self.birthdate(),
            self.email(),
            self.subject(),
            self.startyear(),
            self.hallitus(),
            self.active(),
            self.hallitusRoles().slice()
        );
    };

    self.syncStudent = function(otherStudent) {
        self.id(otherStudent().id());
        self.name(otherStudent().name());
        self.birthdate(otherStudent().birthdate());
        self.email(otherStudent().email());
        self.subject(otherStudent().subject());
        self.startyear(otherStudent().startyear());
        self.hallitus(otherStudent().hallitus());
        self.active(otherStudent().active());
        self.hallitusRoles(otherStudent().hallitusRoles().slice());
        if (!self.hallitus()) {
            self.hallitusRoles([]);
        }
    };

    /**
      * Expects an object in which a key is the property of student object
      * and it's value is the value against which the match will be tested.
      * Returns whether this objects matches the given filters such that
      * the property contains the value of given filter.
    */
    self.matchesContainsFilters = function(filters) {
        var match = true;
        for (var property in filters) {
            if (filters.hasOwnProperty(property)) {
                var filterValue = filters[property];
                var studentProperty = self[property]();
                if (filterValue) {
                    match = studentProperty ? studentProperty.toString().toLowerCase().indexOf(filterValue.toString().toLowerCase()) >= 0 : false;
                }
                if (!match) { 
                    break; 
                }
            }
        }
        return match;
    };

    /**
      * Returns whether student's starting years is between the given range 
      * (inclusive). If only one argument is supplied, returns whether the
      * startyear equals to the given value.
    */
    self.matchesYears = function(start, end) {
        var match = true;
        if (start && end) {
            match = self.startyear() >= start && self.startyear() <= end;
        } else if (start) {
            match = self.startyear() == start;
        } else if (end) {
            match = self.startyear() == end;
        }
        return match;
    };

    self.matchesEqualsFilters = function(equalsFilters) {
        var match = true;         
        for (var property in equalsFilters) {
            if (equalsFilters.hasOwnProperty(property)) {
                var filterValue = equalsFilters[property];
                var studentProperty = self[property]();
                if (filterValue) {
                    match = studentProperty ? studentProperty == filterValue : false;
                }
                if (!match) {
                    break;
                }
            }
        }
        return match;
    };

    self.matchesHallitusFilter = function(role) {
        var found = false;
        ko.utils.arrayForEach(self.hallitusRoles(), function(r) {
            if (r.role == role) {
                found = true;
            }
        });
        return found;
    };
};
