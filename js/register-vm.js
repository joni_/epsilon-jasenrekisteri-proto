var vm = new EditStudentViewModel();

vm.hideResults = function() {
    $('#errors').hide();    
    $('#success').hide();
};

vm.showErrors = function() {
    $('#success').hide();    
    $('#errors').show();
};

vm.showSuccess = function() {
    $('#errors').hide();    
    $('#success').show();
};

vm.submitEdits = function() {
    $.post(epsilonConfig.api.registerUrl, ko.toJSON(vm.editStudent), function(response) {
        var data = JSON.parse(response);
        if (data.success) {
            vm.showSuccess();
        } else {
            vm.showErrors();
        }
    });
};
vm.startEditing(new Student());

epsilonConfig.loadConfig();
var view = new View('register', 'register', vm);
view.loadView();
