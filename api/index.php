<?php
require 'libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array('debug' => true));

// Config
require 'app/config/config.php';
require 'app/data/database.php';
require 'app/data/session.php';
require 'app/data/validation.php';
require 'app/models/student.php';
require 'app/models/administration_entry.php';
require 'app/authentication/auth_middleware.php';
require 'app/authentication/passwordhelper.php';
require 'app/email/emailer.php';

// Routes
require 'app/routes/students.php';
require 'app/routes/auth.php';
require 'app/routes/register.php';
require 'app/routes/config.php';

$app->add(new AuthMiddleware());
$app->run();


