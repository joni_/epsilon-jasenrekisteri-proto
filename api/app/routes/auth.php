<?php
    
$app->group('/auth', function() use ($app) {

    $app->get('/loggedin(/)', function() {
        $session = new Session();
        $loggedIn = $session->isLoggedIn();
        echo json_encode(array('loggedin' => $loggedIn));
    });

    $app->post('/login(/)', function() use ($app) {
        $json = json_decode($app->request->getBody(), true);
        if (!isset($json['username']) || !isset($json['password'])) {
            echo json_encode(array('success' => false, 
                    'message' => 'Invalid parameters'));
            return;
        }

        $username = $json['username'];
        $password = $json['password'];

        $session = new Session();
        $loginSucceeded = $session->login($username, $password);
        if ($loginSucceeded) {
            echo json_encode(array('success' => true, 'message' => 'Logged in'));
        } else {
            echo json_encode(
                array('success' => false, 'message' => 'Invalid credentials'));
        }
    });

    $app->get('/logout(/)', function() {
        $session = new Session();
        $session->logout();
        echo json_encode(array('success' => true, 'message' => 'Logged out'));        
    });

    $app->group('/reset', function() use ($app) {

        $app->post('/init(/)', function() use ($app) {
            $data = json_decode($app->request->getBody(), true);

            $requiredFields = array('email');
            if (!Validation::requiredFieldsFilled($data, $requiredFields)) {
                echo json_encode(array(
                    'success' => false, 'message' => 'Puutteelliset parametrit'));
                return;
            }

            $email = $data['email'];
            $db = new DatabaseHandler();
            $db->connect();
                
            if (!$db->adminExists($email)) {
                echo json_encode(array(
                    'success' => false, 'message' => 'Virheellinen sähköpostiosoite'));
                return;
            }

            $hash = $db->createPasswordResetHash($email);
            $db->disconnect();

            Emailer::sendPasswordResetMail($hash, $email);

            echo json_encode(array('success' => true, 
                'message' => 'Salasanan resetointiohjeet lähetetty sähköpostilla'));
        });
        
        $app->get('/validhash(/)', function() use ($app) {
            $hash = $app->request->get('hash');

            if (empty($hash)) {
                echo json_encode(array(
                    'success' => false, 'message' => 'Puutteelliset parametrit'));
                return;
            }

            $db = new DatabaseHandler();
            $db->connect();
            $valid = $db->validHash($hash);
            $db->disconnect();

            echo json_encode(array('success' => $valid));
        });

        $app->post('/reset(/)', function() use ($app) {
            $data = json_decode($app->request->getBody(), true);

            $requiredFields = array('hash', 'password');
            if (!Validation::requiredFieldsFilled($data, $requiredFields)) {
                echo json_encode(array(
                    'success' => false, 'message' => 'Puutteelliset parametrit'));
                return;
            }

            $hash = $data['hash'];
            $password = $data['password'];

            $db = new DatabaseHandler();
            $db->connect();

            if ($db->validHash($hash)) {
                $success = $db->resetPassword($hash, $password);
                $message = $success ? 'Salasanan resetointi onnistui' : 'Salasanan resetointi epäonnistui';
            } else {
                $success = false;
                $message = 'Salasanan resetointi epäonnistui';
            }

            $db->disconnect();

            echo json_encode(array('success' => $success, 'message' => $message));
        });

    });

});