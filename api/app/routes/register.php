<?php

$app->group('/register', function() use ($app) {

    $app->post('(/)', function() use ($app) {
        $data = json_decode($app->request->getBody(), true);
        $requiredFields = array('id', 'name', 'email', 'birthdate', 
                                'subject', 'startyear');
        
        if (!Validation::requiredFieldsFilled($data, $requiredFields)) {
            echo json_encode(array('success' => false, 'message' => 'Invalid parameters'));
            return;
        }

        $student = new Student($data);
        $db = new DatabaseHandler();
        $db->connect();
        
        if ($db->studentExists($student->id)) {
            echo json_encode(array('success' => false, 'message' => 'Student already exist'));
            return;
        }

        $success = $db->insertPendingStudent($student);
        $db->disconnect();

        $message = 'Registeration done';
        if (!$success) {
            $message = 'Registeration failed';
        }

        // Send confirmation email
        if ($success) {
            Emailer::sendReceivedEmail($student->email);
        }

        echo json_encode(array('success' => $success, 'message' => $message));        
    });

});