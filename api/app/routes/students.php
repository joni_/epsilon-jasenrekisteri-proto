<?php 

$app->group('/students', function() use ($app) {
    
    $app->get('(/)', function() use ($app) {
        $app->contentType('application/json');
        $db = new DatabaseHandler();
        $db->connect();
        $students = $db->getStudents();
        $db->disconnect();  
        echo $students;
    });

    $app->post('(/)', function() use ($app) {
        // Convert the request body JSON into PHP array
        $data = json_decode($app->request->getBody(), true);
        // Student object from the converted array
        $student = new Student($data);

        $db = new DatabaseHandler();
        $db->connect();

        // Modify existing student or add new student if it does not exist in
        // the database.
        if ($db->studentExists($student->id)) {
            $success = $db->modifyStudent($student);
            echo json_encode(array('success' => $success));
        } else {
            $success = $db->addStudent($student);
            echo json_encode(array('success' => $success));
        }

        $db->disconnect();
    });

    $app->post('/remove(/)', function() use ($app) {
        $data = json_decode($app->request->getBody(), true);

        $ids = array();
        foreach ($data as $entry) {
            array_push($ids, (int)$entry['id']);
        }

        $db = new DatabaseHandler();
        $db->connect();
        $success = $db->removeStudents($ids);
        $db->disconnect();
        echo json_encode(array('success' => $success));
    });

    $app->get('/pending(/)', function() use ($app) {
        $app->contentType('application/json');
        $db = new DatabaseHandler();
        $db->connect();
        $students = $db->getPendingStudents();
        $db->disconnect();  
        echo $students;
    });

    $app->post('/pending/accept(/)', function() use ($app) {
        $data = json_decode($app->request->getBody(), true);
        $requiredFields = array('idList');
        if (!Validation::requiredFieldsFilled($data, $requiredFields)) {
            echo json_encode(array('success' => false, 'message' => 'Invalid parameters'));
            return;
        }
        $idList = $data['idList'];
        $db = new DatabaseHandler();
        $db->connect();

        $success = $db->acceptStudent($idList);
        $db->disconnect();

        $message = 'Acceptation succeeded';
        if (!$success) {
            $message = 'Acceptation failed';
        }

        // Send email to the students if acceptation succeeded
        if ($success) {
            foreach ($idList as $id) {
                Emailer::sendAcceptationEmail($id);
            }
        } 

        echo json_encode(array('success' => $success, 'message' => $message));
    });

    $app->post('/pending/reject(/)', function() use ($app) {
        $data = json_decode($app->request->getBody(), true);
        $requiredFields = array('idList');

        if (!Validation::requiredFieldsFilled($data, $requiredFields)) {
            echo json_encode(array('success' => false, 'message' => 'Invalid parameters'));
            return;
        }

        $idList = $data['idList'];
        $db = new DatabaseHandler();
        $db->connect();
        $success = $db->removeStudents($idList);
        $db->disconnect();

        $message = 'Rejection done';
        if (!$success) {
            $message = 'Rejection failed';
        }

        echo json_encode(array('success' => $success, $message => $message));
    });

});
