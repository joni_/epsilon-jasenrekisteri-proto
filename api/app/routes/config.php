<?php

$app->group('/config', function() use ($app) {
    $app->get('(/)', function() {
        $db = new DatabaseHandler();
        $db->connect();
        $roles = $db->getAdministrativeRoles();
        $emails = $db->getEmails();

        $session = new Session();
        $config = array(
            'success' => true,
            'roles' => $roles,
            'emails' => $emails,
        );

        if ($session->isLoggedIn()) {
            $adminEmail = $db->getAdminEmail();
            $config['adminEmail'] = $adminEmail;
        }

        $db->disconnect();
        echo json_encode($config);
    });

    $app->post('(/)', function() use ($app) {
        $data = json_decode($app->request->getBody(), true);
        $db = new DatabaseHandler();
        $db->connect();
        $success = true;
        $message = '';
        if (isset($data['password'])) {
            $success = $db->changePassword($data['password']);
            if (!$success) {
                $message = 'Virheellinen salasana';
            }
        }
        if ($success) {
            $success = $db->setAdminEmail($data['adminEmail']);
        }
        if ($success) {
            $success = $db->setAdministrativeRoles($data['roles']);            
        }
        if ($success) {
            $success = $db->setEmails($data['emails']);
        }
        $db->disconnect();
        echo json_encode(array('success' => $success, 'message' => $message));
    });
});
