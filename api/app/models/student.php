<?php

class Student {
    public $id; 
    public $name; 
    public $birthdate; 
    public $email; 
    public $subject; 
    public $startyear; 
    public $hallitus; 
    public $active;
    public $hallitusRoles;

    public function __construct($dbrow) {
        $this->id        = (int)    $dbrow['id']; 
        $this->name      = (string) $dbrow['name']; 
        $this->birthdate = (string) $dbrow['birthdate']; 
        $this->email     = (string) $dbrow['email']; 
        $this->subject   = (string) $dbrow['subject']; 
        $this->startyear = (int)    $dbrow['startyear']; 
        $this->hallitus  = (bool)   $dbrow['hallitus']; 
        $this->active    = isset($dbrow['active']) ? (bool) $dbrow['active'] : true;
        if (isset($dbrow['active'])) {
        }
        if (isset($dbrow['hallitusRoles'])) {
            $this->hallitusRoles = (array) $dbrow['hallitusRoles'];
        }
    }
}