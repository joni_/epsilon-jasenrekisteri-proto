<?php
class AdministrationEntry {
    public $role;
    public $year;

    public function __construct($dbrow) {
        $this->role = $dbrow['role'];
        $this->year = $dbrow['year'];
    }
}