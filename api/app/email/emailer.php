<?php

class Emailer {

    public static function sendReceivedEmail($email) {
        if (Emailer::allowConfirmationMessages()) {
            $content = Emailer::getReceivedMessage();
            $title = Emailer::getReceivedTitle();
            Emailer::sendMail($email, $title, $content);
        }
    }

    public static function sendAcceptationEmail($student_id) {
        $content = Emailer::getAcceptationEmail();
        $email = Emailer::getStudentEmail($student_id);    
        $title = Emailer::getAcceptationTitle();
        Emailer::sendMail($email, $title, $content);
    }

    public static function sendPasswordResetMail($hash, $email) {
        $title = "Salasanan resetointi";
        $url = PASSWORD_RESET_URL . '?h=' . $hash;
        $content = "Resetoi salasana osoitteessa $url. Linkki toimii tunnin ajan.";
        Emailer::sendMail($email, $title, $content);
    }

    private static function allowConfirmationMessages() {
        $db = new DatabaseHandler();
        $db->connect();
        $message = $db->allowConfirmationMessages();
        $db->disconnect();
        return $message;        
    }

    private static function getReceivedTitle() {
        $db = new DatabaseHandler();
        $db->connect();
        $title = $db->getReceivedTitle();
        $db->disconnect();
        return $title;
    }

    private static function getReceivedMessage() {
        $db = new DatabaseHandler();
        $db->connect();
        $message = $db->getReceivedMessage();
        $db->disconnect();
        return $message;
    }

    private static function getAcceptationTitle() {
        $db = new DatabaseHandler();
        $db->connect();
        $title = $db->getAcceptationTitle();
        $db->disconnect();
        return $title;
    }

    private static function getAcceptationEmail() {
        $db = new DatabaseHandler();
        $db->connect();
        $message = $db->getAcceptationEmail();
        $db->disconnect();
        return $message;
    }

    private static function getStudentEmail($student_id) {
        $db = new DatabaseHandler();
        $db->connect();
        $email = $db->getStudentEmail($student_id);
        $db->disconnect();
        return $email;
    }


    private static function sendMail($email, $title, $content) {
        $headers = "From: epsilon@epsilonry.fi\r\n";
        mail($email, $title, $content, $headers);
    }

}