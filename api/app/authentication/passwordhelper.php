<?php

class PasswordHelper {
    public static function passwordsMatch($hashedPw, $password) {
        return password_verify($password, $hashedPw);
    }

    public static function getHashedPassword($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}