<?php

class AuthMiddleware extends \Slim\Middleware
{
    /**
     * This middleware layer checks whether call to requested path is allowed.
     * If user is not authenticated, only calls to /auth/login, /auth/loggedin,
     * /auth/logout and /register are allowed. 
     */
    public function call()
    {
        $pathInfo = $this->app->request->getPathInfo();
        
        $allowedPaths = ['/auth/login', '/auth/loggedin', '/auth/logout',
                         '/register', '/config', '/auth/reset/init', 
                         '/auth/reset/validhash', '/auth/reset/reset'];
        $authorized = in_array($pathInfo, $allowedPaths);
        
        $session = new Session();
        if ($authorized || $session->isLoggedIn()) {
            $this->next->call();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Unauthorized'));
        }
    }
}
