<?php 

class DatabaseHandler {
    private $connection;

    public function connect() {
        $this->connection = new PDO(
            "mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE . ";charset=utf8",
            DB_USERNAME, DB_PASSWORD);
    }

    public function disconnect() {
        $this->connection = null;
    }

    public function getPendingStudents() {
        return $this->getStudents(true);
    }

    public function getStudents($pending = false) {
        $select = "SELECT 
                    students.student_id as id,
                    students.name, 
                    (SELECT 
                        subjects.subject 
                        FROM subjects 
                        WHERE students.subject = subjects.id
                    ) as subject,
                    students.email,
                    DATE_FORMAT(students.birthdate,'%d.%m.%Y') as birthdate,
                    students.startyear,
                    students.active,
                    (SELECT 
                        count(1) 
                        FROM administration
                        WHERE administration.student_id = students.student_id
                    ) as hallitus
                FROM students
                WHERE pending = " . ($pending ? 1 : 0);

        $query = $this->executeQueryAndGetHandle($select, array());

        $students = array();
        while ($row = $query->fetch()) {
            $student = new Student($row);
        
            $administration_roles = 
                $this->getStudentsAdministrationRoles($student->id);
            $student->hallitusRoles = $administration_roles;

            array_push($students, $student);
        }

        return json_encode($students);
    }

    public function studentExists($student_id) {
        $select = "SELECT * FROM students WHERE student_id = :student_id";
        $query = $this->executeQueryAndGetHandle(
            $select, array('student_id' => $student_id));
        return $query->rowCount() > 0;
    }

    public function modifyStudent($student) {
        $subject_id = $this->getSubjectId($student->subject);

        // Transaction to make sure both updating student and administration
        // roles will work.
        $this->connection->beginTransaction();

        $update = "UPDATE students 
                    SET student_id=:student_id,
                        name=:name,
                        email=:email,
                        birthdate=:birthdate,
                        subject=:subject,
                        startyear=:startyear,
                        active=:active
                    WHERE student_id = :student_id";

        $mapping = $this->getParameterMapping($student);
        $mapping['subject'] = $subject_id;

        $success = $this->executeQuery($update, $mapping);

        if ($success) {
            $success = $this->modifyAdministrationRoles($student);
        }

        if ($success) {
            $this->connection->commit();
        } else {
            $this->connection->rollBack();
        }

        return $success;
    }

    public function addStudent($student) {
        $subject_id = $this->getSubjectId($student->subject);
        $this->connection->beginTransaction();

        $insert = "INSERT INTO students
                    (
                        student_id, name, email, birthdate, 
                        subject, startyear, active
                    )
                    VALUES 
                    (
                        :student_id, :name, :email, :birthdate, 
                        :subject, :startyear, :active
                    )";
        $mapping = $this->getParameterMapping($student);
        $mapping['subject'] = $subject_id;

        $success = $this->executeQuery($insert, $mapping);
        
        if ($success) {
            $success = $this->modifyAdministrationRoles($student);
        }

        if ($success) {
            $this->connection->commit();
        } else {
            $this->connection->rollBack();
        }
        
        return $success;
    }

    private function getParameterMapping($student) {
        $birthdate = DateTime::createFromFormat('d.m.Y', $student->birthdate);
        return array(
            'student_id' => $student->id,
            'name' => $student->name,
            'email' => $student->email,
            'birthdate' => $birthdate->format('Y-m-d'),
            'subject' => $student->subject,
            'startyear' => $student->startyear,
            'active' => $student->active
        );        
    }

    public function removeStudents($ids) {
        $idString = implode(",", $ids);
        $delete = "DELETE FROM students WHERE FIND_IN_SET(student_id, :student_id)";
        return $this->executeQuery($delete, array('student_id' => $idString));
    }

    private function getStudentsAdministrationRoles($student_id) {
        $select = "SELECT
                    (SELECT role
                        FROM administration_roles
                        WHERE administration_roles.id = administration.role
                    ) as role,
                    year
                    FROM administration
                    WHERE administration.student_id = :student_id
                    AND (
                        SELECT active
                        FROM administration_roles
                        WHERE administration_roles.id = administration.role
                    ) = 1";
        $query = $this->executeQueryAndGetHandle(
            $select, array('student_id' => $student_id));

        $roles = array();
        while ($row = $query->fetch()) {
            array_push($roles, new AdministrationEntry($row));
        }

        return $roles;
    }

    private function modifyAdministrationRoles($student) {
        // Remove all active entries for given student
        $delete = "DELETE 
                    FROM administration 
                    WHERE student_id = :student_id
                    AND (
                        SELECT active 
                        FROM administration_roles 
                        WHERE administration_roles.id = administration.role
                    ) = 1;";
        if (!$this->executeQuery($delete, array('student_id' => $student->id))) {
            return false;
        }

        // Insert each role in the student object into DB again
        foreach ($student->hallitusRoles as $entry) {
            $role = $entry['role'];
            $year = $entry['year'];
            $role_id = $this->getRoleId($role);

            // Add all records in student object to DB
            $insert = "INSERT INTO administration (student_id, role, year) 
                        VALUES (:student_id, :role, :year)";
            $mapping = array(
                'student_id' => $student->id, 
                'role' => $role_id, 
                'year' => $year
            );

            if (!$this->executeQuery($insert, $mapping)) {
                return false;
            }
        }

        return true;
    }

    private function getSubjectId($subject) {
        $select = "SELECT id FROM subjects WHERE subject = :subject";
        $row = $this->getFirstRow($select, array('subject' => $subject));
        return $row['id'];
    }

    private function getRoleId($role) {
        $select = "SELECT id FROM administration_roles WHERE role = :role";
        $row = $this->getFirstRow($select, array('role' => $role));
        return $row['id'];
    }

    private function executeQuery($query, $mapping) {
        if (!$this->connection) {
            throw new Exception("No database connection established", 1);
        }

        $stmt = $this->connection->prepare($query);
        return $stmt->execute($mapping);
    }

    private function executeQueryAndGetHandle($query, $mapping) {
        if (!$this->connection) {
            throw new Exception("No database connection established", 1);
        }

        $stmt = $this->connection->prepare($query);
        $stmt->execute($mapping);
        return $stmt;
    }

    private function getFirstRow($query, $mapping) {
        $stmt = $this->executeQueryAndGetHandle($query, $mapping);
        if ($stmt->rowCount() === 0) {
            throw new Exception("No rows were found", 1);
        }

        return $stmt->fetch();
    }

    public function getUserRow($username) {
        $select = "SELECT * FROM users WHERE username = :username";
        return $this->getFirstRow($select, array('username' => $username));
    }

    public function insertPendingStudent($student) {
        $subject_id = $this->getSubjectId($student->subject);
        $insert = "INSERT INTO students 
                    (
                        student_id,
                        name,
                        email,
                        birthdate,
                        subject,
                        startyear,
                        active,
                        pending
                    )
                    VALUES 
                    (
                        :student_id,
                        :name,
                        :email,
                        :birthdate,
                        :subject,
                        :startyear,
                        :active,
                        1
                    )";
    
        $mapping = $this->getParameterMapping($student);
        $mapping['subject'] = $subject_id;
        
        $this->connection->beginTransaction();

        $success = $this->executeQuery($insert, $mapping);

        if ($success) {
            $success = $this->modifyAdministrationRoles($student);
        }

        if ($success) {
            $this->connection->commit();
        } else {
            $this->connection->rollBack();
        }

        return $success;
    }

    public function acceptStudent($idList) {
        $ids = implode(",", $idList);
        $this->connection->beginTransaction();
        $success = true;
        $update = "UPDATE students SET pending = 0 WHERE student_id = :student_id";
        foreach ($idList as $id) {
            $success = $this->executeQuery($update, array('student_id' => $id));
            if (!$success) {
                break;
            }
        }
        if ($success) {
            $this->connection->commit();
        } else {
            $this->connection->rollBack();
        }
        return $success;
    }

    public function getAdministrativeRoles() {
        $select = "SELECT role, active FROM administration_roles";
        $query = $this->executeQueryAndGetHandle($select, array());

        $roles = array();
        while ($row = $query->fetch()) {
            $role = array('role' => $row['role'], 'active' => (bool)$row['active']);
            array_push($roles, $role);
        }

        return $roles;
    }

    public function setAdministrativeRoles($roles) {
        $update = "UPDATE administration_roles SET active = :active WHERE role = :role";
        $insert = "INSERT INTO administration_roles (role, active) 
                   VALUES (:role, :active)";

        $this->connection->beginTransaction();

        $success = false;
        foreach ($roles as $role) {
            $name = $role['role'];
            $active = $role['active'];

            $query = $this->roleExists($name) ? $update : $insert;
            $success = $this->executeQuery($query, 
                array('role' => $name, 'active' => $active));
            
            if (!$success) {
                break;
            }
        }

        if ($success) {
            $this->connection->commit();
        } else {
            $this->connection->rollBack();
        }

        return $success;
    }

    public function roleExists($rolename) {
        $select = "SELECT * FROM administration_roles WHERE role = :role";
        $query = $this->executeQueryAndGetHandle($select,
            array('role' => $rolename));
        return $query->rowCount() > 0;
    }

    public function setEmails($emails) {
        $acceptEmail = $emails['accepted'];
        $receivedEmail = $emails['received'];

        $update = "UPDATE emails 
                    SET title = :title, content = :content, sendmail = :sendmail
                    WHERE type = :type";

        $success = $this->executeQuery($update, array(
            'title' => $acceptEmail['title'],
            'content' => $acceptEmail['content'],
            'sendmail' => $acceptEmail['sendmail'],
            'type' => 'accept'
        ));

        if ($success) {
            $success = $this->executeQuery($update, array(
                'title' => $receivedEmail['title'],
                'content' => $receivedEmail['content'],
                'sendmail' => $receivedEmail['sendmail'],
                'type' => 'received'
            ));
        }

        return $success;
    }

    public function getEmails() {
        $select = "SELECT * FROM emails";
        $query = $this->executeQueryAndGetHandle($select, array());

        $emails = array();
        while ($row = $query->fetch()) {
            $email = array('title' => $row['title'], 
                           'content' => $row['content'], 
                           'sendmail' => (bool)$row['sendmail']);
            $emails[$row['type']] = $email;
        }
        return $emails;
    }

    public function changePassword($password) {
        if (!$this->validPassword($password)) {
            return false;
        }

        $hash = PasswordHelper::getHashedPassword($password);
        $session = new Session();
        $username = $session->getUserName();
        $update = "UPDATE users SET password = :password WHERE username = :username";
        return $this->executeQuery($update, array(
            'username' => $username, 'password' => $hash));
    }

    public function resetPassword($resethash, $password) {
        if (!$this->validPassword($password)) {
            return false;
        }

        $hash = PasswordHelper::getHashedPassword($password);
        $update = "UPDATE users SET password = :password WHERE resethash = :resethash";
        $success = $this->executeQuery($update, array('password' => $hash, 'resethash' => $resethash));
        
        if ($success) {
            $update = "UPDATE users SET resethash = NULL, resettime = NULL WHERE resethash = :resethash";
            $this->executeQuery($update, array('resethash' => $resethash));
        }

        return $success;
    }

    public function validPassword($password) {
        return strlen($password) >= 8;
    }

    public function getAdminEmail() {
        $session = new Session();
        $username = $session->getUserName();
        $row = $this->getUserRow($username);
        return $row['email'];
    }

    public function setAdminEmail($email) {
        $session = new Session();
        $username = $session->getUserName();
        $update = "UPDATE users SET email = :email WHERE username = :username";
        return $this->executeQuery($update, array(
            'email' => $email, 'username' => $username));
    }

    public function getReceivedTitle() {
        $select = "SELECT title FROM emails WHERE type = 'received';";
        $row = $this->getFirstRow($select, array());
        return $row['title'];
    }

    public function getReceivedMessage() {
        $select = "SELECT content FROM emails WHERE type = 'received';";
        $row = $this->getFirstRow($select, array());
        return $row['content'];
    }

    public function allowConfirmationMessages() {
        $select = "SELECT sendmail FROM emails WHERE type = 'received'";
        $row = $this->getFirstRow($select, array());
        return (bool) $row['sendmail'];
    }

    public function getAcceptationTitle() {
        $select = "SELECT title FROM emails WHERE type = 'accept';";
        $row = $this->getFirstRow($select, array());
        return $row['title'];
    }

    public function getAcceptationEmail() {
        $select = "SELECT content FROM emails WHERE type = 'accept';";
        $row = $this->getFirstRow($select, array());
        return $row['content'];
    }

    public function getStudentEmail($student_id) {
        $select = "SELECT email FROM students WHERE student_id = :student_id";
        $row = $this->getFirstRow($select, array('student_id' => $student_id));
        return $row['email'];
    }

    public function adminExists($email) {
        try {
            // getFirstRow throws an exception is no rows are found
            $select = "SELECT * FROM users WHERE email = :email";
            $this->getFirstRow($select, array('email' => $email));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function createPasswordResetHash($email) {
        $hash = sha1(openssl_random_pseudo_bytes(16));

        $update = "UPDATE users SET 
                       resethash = :resethash,
                       resettime = NOW()
                   WHERE email = :email";
        $this->executeQuery($update, array('resethash' => $hash, 'email' => $email));
        return $hash;
    }

    public function validHash($hash) {
        try {
            $select = "SELECT 
                        resethash, 
                        date_add(resettime, INTERVAL 1 HOUR) >= NOW() as valid_time 
                        FROM users 
                        WHERE resethash = :hash";
            $row = $this->getFirstRow($select, array('hash' => $hash));
            return (bool) $row['valid_time'];
        } catch (Exception $e) {
            return false;
        }
    }
}
