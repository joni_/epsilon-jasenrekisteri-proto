<?php

class Validation {
    public static function requiredFieldsFilled($input, $requiredFields) {
        // Check that all fields are set and not empty
        foreach ($requiredFields as $field) {
            if (!isset($input[$field])) {
                return false;
            }
            if (empty($input[$field])) {
                return false;
            }
        }
        return true;
    }
}