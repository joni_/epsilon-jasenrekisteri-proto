<?php

class Session {
    public function __construct() {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function login($username, $password) {
        $db = new DatabaseHandler();
        $db->connect();

        // Exception is thrown if username is not found in DB
        try {
            $row = $db->getUserRow($username);
        } catch (Exception $e) {
            return false;
        } finally {
            $db->disconnect();
        }

        $hashedPassword = $row['password'];
       
        $loggedIn = PasswordHelper::passwordsMatch($hashedPassword, $password);
        if ($loggedIn) {
            $_SESSION['username'] = $username;
        }
        return $loggedIn;
    }

    public function logout() {
        unset($_SESSION['username']);
        session_destroy();
    }

    public function isLoggedIn() {
        return isset($_SESSION) && isset($_SESSION['username']) && 
            !empty($_SESSION['username']);
    }

    public function getUserName() {
        if ($this->isLoggedIn()) {
            return $_SESSION['username'];
        }
    }
}